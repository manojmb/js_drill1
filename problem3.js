// Function sortAlphabetically takes array inventory as input and returns the car model alphabetically
function sortAlphabetically(inventory) {
    // Checks whether inventory is array or not
    if (Array.isArray(inventory)) {
        // Empty names array for car model name
        let names = [];
        let minIndex = 0;
        // Using Insertion sort to sort alphabetically
        while (inventory.length > 0) {
            for (let index = 1; index < inventory.length; index++) {
                if (inventory[index].car_model.toLowerCase() < inventory[minIndex].car_model.toLowerCase()) {
                    minIndex = index;
                }
            }
            names.push(inventory[minIndex].car_model);
            // Removes item at index minIndex
            inventory.splice(minIndex, 1);
            minIndex = 0;
        }
        // Return names array which is sorted
        return names;
    }
    // Returns statement if  input is not array
    return "Please give array as input";
}

// The below statement exports function sortAlphabetically which can be imported in other files
module.exports = { sortAlphabetically };