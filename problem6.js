// Function findBMWandAudi takes array inventory and return array of car manufactured before 2000
function findBMWandAudi(inventory) {
    bmw_audi_array = [];
    if (Array.isArray(inventory)) {
        for (let index = 0; index < inventory.length; index++) {
            if (inventory[index].car_make.toLowerCase() === "audi" || inventory[index].car_make.toLowerCase() === "bmw") {
                bmw_audi_array.push(inventory[index]);
            }
        }
        return JSON.stringify(bmw_audi_array);
    }
}
// The below statement exports function problem4 which can be imported in other files
module.exports = { findBMWandAudi };