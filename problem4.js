// Function findCarYears takes array inventory as input and returns the array of car years
function findCarYearsAndDetails(inventory) {
    // An empty array for car years
    car_year = [];
    car_details = [];
    // Check whether inventory is array or not
    if (Array.isArray(inventory)) {
        // Loop through inventory year
        for (let index = 0; index < inventory.length; index++) {
            // Push car details to car_details array
            car_details.push(`Id ${inventory[index].id} ${inventory[index].car_model} ${inventory[index].car_make}`);
            // Push car year to car_year array
            car_year.push(inventory[index].car_year);
        }
    }
    // Returns array of car_year and car details combined in form of array
    return [car_year, car_details];
}

// The below statement exports function findCarYearsAndDetails which can be imported in other files
module.exports = { findCarYearsAndDetails };