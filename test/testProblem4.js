// Importing findCarYearsAndDetails function from problem4.js
const { findCarYearsAndDetails } = require("../problem4.js");
// Importing inventory data for inventory_data.js
const { inventory } = require("./inventory_data.js");
try {
    // Result array returned from running function problem 4
    result = findCarYearsAndDetails(inventory);
    // Iterating through result array to print only years which is stored in result[0]
    for (let index = 0; index < result[0].length; index++) {
        console.log(result[0][index]);
    }
} catch (e) {
    // Logs error if encounters any
    console.log(e.message);
}