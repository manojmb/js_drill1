// Importing findCarsBeforeYear function from problem5.js
const { findCarsBeforeYear } = require("../problem5.js");
// Importing inventory data for inventory_data.js
const { inventory } = require("./inventory_data.js");
try {
    // Cars before year to be found
    year = 2000;
    result = findCarsBeforeYear(inventory, year);
    console.log(`Number of car manufactured before ${year} is ${result[0]}`);
    console.log(`The cars before year ${year} are :`);
    for (let index = 0; index < result[1].length; index++) {
        console.log(result[1][index], `in`, result[2][index]);
    }
} catch (e) {
    // Logs error if encounters any
    console.log(e.message);
}
