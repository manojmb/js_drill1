// Here we use findCarYearsAndDetailsfunction to get array of cars so we import function from problem4.js
const { findCarYearsAndDetails } = require("./problem4.js");
// Function findCarsBeforeYear takes array inventory and return array of car manufactured before 2000
function findCarsBeforeYear(inventory, year) {
    // Checks whether inventory is array
    if (Array.isArray(inventory)) {
        // year array of cars to save all car years got by running findCarYearsAndDetailsfunction
        year_array = findCarYearsAndDetails(inventory)[0];
        // car array of cars to save all car details got by running findCarYearsAndDetailsfunction
        car_array = findCarYearsAndDetails(inventory)[1];
        // empty before year array to save car years manufactured before 2000
        before_year_array = [];
        // empty before car array to save car details of cars manufactured before 2000
        before_car_array = [];
        // Iterating through array to get car details manufacture before 2000 and pushing it in new array
        for (let index = 0; index < year_array.length; index++) {
            if (Number(year_array[index]) < Number(year)) {
                before_year_array.push(year_array[index]);
                before_car_array.push(car_array[index]);
            }
        }
        // Returns array of number of car ,car details and years of car manufactured before 2000
        return [before_year_array.length, before_car_array, before_year_array];
    }
}
// The below statement exports function findCarsBeforeYear which can be imported in other files
module.exports = { findCarsBeforeYear };