// Creating a function named findCarById which takes inventory data and id of car to be found
function findCarById(inventory, id) {
    // Checks whether inventory is array or not and id is less than 51
    if (Array.isArray(inventory) && Number(id) < 51) {
        for (let index = 0; index < inventory.length; index++) {
            // Checks whether inventory id is equal to passed id
            if (inventory[index].id === Number(id)) {
                // Returns a statement with car id car year and car model if found
                return `Car ${id} is a ${inventory[index].car_year}  ${inventory[index].car_make} ${inventory[index].car_model}`;
            }
        }
    }
    // Returns below statement if above are not satisfied
    return "No such car or input is not array";
}
// The below statement exports function findCarById which can be imported in other files
module.exports = { findCarById };